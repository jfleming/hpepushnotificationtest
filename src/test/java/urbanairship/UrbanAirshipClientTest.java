
package urbanairship;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.testng.Assert;
//import org.testng.annotations.*;

import com.google.gson.Gson;

public class UrbanAirshipClientTest
{
	private String username = "Venkatesh28";
	private String password = "Nttdata@123";
	private String deviceAlias;
	//@BeforeTest
	public void setUp()
	{
		String usernameProperty = (String) System.getProperties().get("urbanairship.java.username");
		if (usernameProperty != null)
		{
			username = usernameProperty;
		}
		
		String passwordProperty = (String) System.getProperties().get("urbanairship.java.password");
		if (passwordProperty != null)
		{
			password = passwordProperty;
		}
	}
	
	//@Test(enabled=false)
	/*public void client()
	{
		//UrbanAirshipClient client = new UrbanAirshipClient("yAzI8H9IS_eCgHDSbmmSSQ",
		        //"mYJo1-CtRYyfMrc65_umnA");
		
		Device dev = new Device();
		dev.setiOSDeviceToken("821299783a425b1d29b5e35f70dda3fcc74713cefe860e16f758a32c44aabcee");
		dev.setAlias("ADM");
		client.register(dev);		
		
		
	}*/
	
	
	//@Test
	public void jsonPush()
	{
		APS aps = new APS();
		aps.setAlert("You have a new Explanation of Benefits Summary to review.");
		aps.setSound("default");
					
		Push p = new Push();
		p.setAps(aps);
		p.addPayloadValue("test", "eobsummary.html");
		
		List<String> aliases = new ArrayList<String>();
		aliases.add(0, "081625");
		 
		p.setAliases(aliases);
				
		Gson gson = GsonFactory.create();
		String json = gson.toJson(p);
	    Assert.assertNotNull(json);

		System.out.println("Push: " + json);
		//UrbanAirshipClient uac = new UrbanAirshipClient("yAzI8H9IS_eCgHDSbmmSSQ", "mYJo1-CtRYyfMrc65_umnA");
		UrbanAirshipClient uac = new UrbanAirshipClient("zVNWYVmURYq5bwQyr1c_aw", "66BxjIVBSbielu2_uEzvQQ");
		
		uac.sendPushNotifications(p);
		
	}
	
	//@Test
	public void jsonCalendar()
	{
		APS aps = new APS();
		
		aps.setAlert("You've got mail!");
		aps.setBadge(1);
		
		Calendar c = Calendar.getInstance();

		c.add(Calendar.DAY_OF_YEAR, 1);
		c.set(Calendar.MILLISECOND, 0);
		
		Broadcast bcast = new Broadcast();
		bcast.setScheduleFor(c);

		Assert.assertNotNull(bcast.getScheduleFor());
		
		
		bcast.setAps(aps);
		
		Gson gson = GsonFactory.create();
		
		String json = gson.toJson(bcast);
		
		Assert.assertNotNull(json);

		System.out.println("Broadcast: " + json);
		
		Broadcast bcast2 = gson.fromJson(json, Broadcast.class);
		
		Assert.assertNotNull(bcast2);
		
		Assert.assertNotNull(bcast2.getScheduleFor());
		
		Assert.assertEquals(bcast.getScheduleFor().getTimeInMillis(), 
							bcast2.getScheduleFor().getTimeInMillis());
		
		
	}
	
}
